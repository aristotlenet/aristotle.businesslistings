﻿using System;
using System.Linq;
using System.Web.Mvc;
using Aristotle.BusinessListings.Models;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Indexing;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Settings;
using Orchard.Themes;
using Orchard.UI.Navigation;

namespace Aristotle.BusinessListings.Controllers
{
    [Themed]
    public class BusinessListingSearchController : Controller
    {
         private readonly IContentManager _contentManager;
        private readonly ISiteService _siteService;
        private readonly IIndexProvider _indexProvider;

        public BusinessListingSearchController(
            IContentManager contentManager, 
            IShapeFactory shapeFactory,
            ISiteService siteService,
            IIndexProvider indexProvider)
        {
            _contentManager = contentManager;
            _siteService = siteService;
            _indexProvider = indexProvider;
            Logger = NullLogger.Instance;
            Shape = shapeFactory;
            T = NullLocalizer.Instance;
        }

        dynamic Shape { get; set; }
        protected ILogger Logger { get; set; }
        public Localizer T { get; set; }

        public ActionResult List(PagerParameters pagerParameters, string keywords, string businessCategory) {
            var builder = _indexProvider.CreateSearchBuilder("BusinessListing");
            

            if (!String.IsNullOrEmpty(keywords)) {
                builder.WithField("title", keywords).AsFilter();
                builder.WithField("body", keywords).AsFilter();
            }

            if (!String.IsNullOrEmpty(businessCategory))
            {
                builder.WithField("BusinessCategory", businessCategory.Split('/').Last()).AsFilter();
            }
            builder.SortByString("title");

            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);
            var businessListingCount = builder.Count();
            var contentItemIds = builder
                .Slice(pager.GetStartIndex(), pager.PageSize)
                .Search()
                .Select(x => x.ContentItemId)
                .ToArray();

            var query = _contentManager.GetMany<BusinessListingPart>(contentItemIds, VersionOptions.Published, QueryHints.Empty);

            var search = _contentManager.New("BusinessListingSearch");
            search.Weld(new BusinessListingSearchPart());

            var searchShape = _contentManager.BuildDisplay(search);
            var list = Shape.List();
            list.AddRange(query.Select(e => _contentManager.BuildDisplay(e, "Summary")));

            var viewModel = Shape.ViewModel()
                .BusinessListingSearch(searchShape.BusinessListingSearch)
                .ContentItems(list)
                .Pager(Shape.Pager(pager).TotalItemCount(businessListingCount));

            return View("BusinessListing/List", viewModel);
        }
    }
}