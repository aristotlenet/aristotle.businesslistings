﻿using System;
using System.Linq;
using System.Web.Mvc;
using Aristotle.BusinessListings.Models;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Settings;
using Orchard.Themes;
using Orchard.UI.Navigation;

namespace Aristotle.BusinessListings.Controllers
{
    [Themed]
    public class BusinessListingController : Controller
    {
        private readonly IContentManager _contentManager;
        private readonly ISiteService _siteService;

        public BusinessListingController(
            IContentManager contentManager,
            IShapeFactory shapeFactory,
            ISiteService siteService)
        {
            _contentManager = contentManager;
            _siteService = siteService;
            Logger = NullLogger.Instance;
            Shape = shapeFactory;
            T = NullLocalizer.Instance;
        }

        dynamic Shape { get; set; }
        protected ILogger Logger { get; set; }
        public Localizer T { get; set; }

        public ActionResult List(PagerParameters pagerParameters)
        {
            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);
            var query = _contentManager.Query<BusinessListingPart>(VersionOptions.Published);
           
            var pageOfEvents = query.Slice(pager.GetStartIndex(), pager.PageSize).ToList();

            var search = _contentManager.New("BusinessListingSearch");
            search.Weld(new BusinessListingSearchPart());

            var searchShape = _contentManager.BuildDisplay(search);

            var list = Shape.List();
            list.AddRange(pageOfEvents.Select(e => _contentManager.BuildDisplay(e, "Summary")));

            dynamic viewModel = Shape.ViewModel()
                .BusinessListingSearch(searchShape.BusinessListingSearch)
                .ContentItems(list)
                .Pager(Shape.Pager(pager).TotalItemCount(query.Count()));

            return View(viewModel);
        }
    }
}