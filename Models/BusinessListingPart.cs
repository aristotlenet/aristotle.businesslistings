﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class BusinessListingPart : ContentPart<BusinessListingPartRecord>
    {
        public string Title
        {
            get { return this.As<TitlePart>().Title; }
            set { this.As<TitlePart>().Title = value; }
        }

        public string Description
        {
            get { return this.As<BodyPart>().Text; }
            set { this.As<BodyPart>().Text = value; }
        }

        public string Phone
        {
            get { return Record.Phone; }
            set { Record.Phone = value; }
        }

        public string Email
        {
            get { return Record.Email; }
            set { Record.Email = value; }
        }

        public string Website
        {
            get { return Record.Website; }
            set { Record.Website = value; }
        }

        public string TollFree
        {
            get { return Record.TollFree; }
            set { Record.TollFree = value; }
        }

        public string Fax
        {
            get { return Record.Fax; }
            set { Record.Fax = value; }
        }

        public string BookingUrl
        {
            get { return Record.BookingUrl; }
            set { Record.BookingUrl = value; }
        }

        public string Address
        {
            get { return Record.Address; }
            set { Record.Address = value; }
        }

        public string Address2
        {
            get { return Record.Address2; }
            set { Record.Address2 = value; }
        }

        public CityPartRecord City
        {
            get { return Record.City; }
            set { Record.City = value; }
        }

        public string State
        {
            get { return Record.State; }
            set { Record.State = value; }
        }

        public string ZipCode
        {
            get { return Record.ZipCode; }
            set { Record.ZipCode = value; }
        }

        public string GpsLat
        {
            get { return Record.GpsLat; }
            set { Record.GpsLat = value; }
        }

        public string GpsLng
        {
            get { return Record.GpsLng; }
            set { Record.GpsLng = value; }
        }

        public string CvbId
        {
            get { return Record.CvbId; }
            set { Record.CvbId = value; }
        }

        public bool Featured
        {
            get { return Record.Featured; }
            set { Record.Featured = value; }
        }

        public DateTime? PublishedDate
        {
            get { return this.As<ICommonPart>().PublishedUtc; }
        }
    }
}