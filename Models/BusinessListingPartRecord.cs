﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class BusinessListingPartRecord : ContentPartRecord
    {
        // business name as Title
        // business description as Body
        // contact info in a ContactPart
        // images/gallery from standard Image Gallery part
        // region/geo information from taxonomies
        // meta data in SeoMetaPart

        public virtual string Phone { get; set; }
        public virtual string TollFree { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual string BookingUrl { get; set; }

        public virtual string Address { get; set; }
        public virtual string Address2 { get; set; }
        public virtual CityPartRecord City { get; set; }
        public virtual string State { get; set; }
        public virtual string ZipCode { get; set; }

        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }

        public virtual bool Featured { get; set; }
        public virtual string CvbId { get; set; }

    }
}