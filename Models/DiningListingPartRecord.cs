﻿using System;
using Orchard.ContentManagement.Records;


namespace Aristotle.BusinessListings.Models
{
    public class DiningListingPartRecord : ContentPartRecord
    {
        //many fields come from the attached BusinessListingPart
        // dining amenities and categories from taxonomies

        public virtual int SeatingCapacity { get; set; }
       
    }
}