﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class WaterPartRecord : ContentPartRecord
    {
        // name, description, etc. are other objects
        public virtual string Phone { get; set; }
        public virtual string TollFree { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
       
        public virtual string Address { get; set; }
        public virtual string Address2 { get; set; }
        public virtual CityPartRecord City { get; set; }
        public virtual string State { get; set; }
        public virtual string ZipCode { get; set; }

    }
}