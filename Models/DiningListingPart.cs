﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class DiningListingPart : ContentPart<DiningListingPartRecord>
    {
        public int SeatingCapacity
        {
            get { return Record.SeatingCapacity; }
            set { Record.SeatingCapacity = value; }
        }
    }
}