﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class SeoMetaPart : ContentPart<SeoMetaPartRecord>
    {
        public string Description
        {
            get { return Record.Description; }
            set { Record.Description = value; }
        }
        public string Keywords
        {
            get { return Record.Keywords; }
            set { Record.Keywords = value; }
        }
      
        public string OgImage
        {
            get { return Record.OgImage; }
            set { Record.OgImage = value; }
        }
        public string OtherHeader
        {
            get { return Record.OtherHeader; }
            set { Record.OtherHeader = value; }
        }
    }
}