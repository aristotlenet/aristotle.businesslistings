﻿using System;
using Orchard.ContentManagement.Records;


namespace Aristotle.BusinessListings.Models
{
    public class LodgingListingPartRecord : ContentPartRecord
    {
        //many fields come from the attached BusinessListingPart
        // lodging amenities and categories from taxonomies

        public virtual int NumberOfRooms { get; set; }
    }
}