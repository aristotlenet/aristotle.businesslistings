﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class CityPartRecord : ContentPartRecord
    {
        // name, description, county, region, photos come from other objects

        public virtual string Name { get; set; }
        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }
        public virtual int Zoom { get; set; }
    }
}