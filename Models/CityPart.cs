﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class CityPart : ContentPart<CityPartRecord>
    {
        public string Title
        {
            get { return this.As<TitlePart>().Title; }
            set { this.As<TitlePart>().Title = value; }
        }

        public string Description
        {
            get { return this.As<BodyPart>().Text; }
            set { this.As<BodyPart>().Text = value; }
        }

        public string GpsLat
        {
            get { return Record.GpsLat; }
            set { Record.GpsLat = value; }
        }

        public string GpsLng
        {
            get { return Record.GpsLng; }
            set { Record.GpsLng = value; }
        }

        public int Zoom
        {
            get { return Record.Zoom; }
            set { Record.Zoom = value; }
        }

        public string Name
        {
            get { return Record.Name; }
            set { Record.Name = value; }
        }
    }
}