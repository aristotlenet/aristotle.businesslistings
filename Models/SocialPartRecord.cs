﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class SocialPartRecord: ContentPartRecord
    {
        public virtual string FacebookLink { get; set; }
        public virtual string TwitterLink { get; set; }
        public virtual string PinterestLink { get; set; }
        public virtual string GooglePlusLink { get; set; }
    }
}