﻿using System;
using Orchard.ContentManagement.Records;


namespace Aristotle.BusinessListings.Models
{
    public class ContactPartRecord : ContentPartRecord
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Company { get; set; }
        public virtual string Address { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
    }
}