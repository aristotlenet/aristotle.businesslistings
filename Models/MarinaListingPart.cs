﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class MarinaListingPart : ContentPart<MarinaListingPartRecord>
    {
        public string Hours
        {
            get { return Record.Hours; }
            set { Record.Hours = value; }
        }
    }
}