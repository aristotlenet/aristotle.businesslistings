﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class SocialPart : ContentPart<SocialPartRecord>
    {
        public string FacebookLink
        {
            get { return Record.FacebookLink; }
            set { Record.FacebookLink = value; }
        }
        public string TwitterLink
        {
            get { return Record.TwitterLink; }
            set { Record.TwitterLink = value; }
        }
        public string PinterestLink
        {
            get { return Record.PinterestLink; }
            set { Record.PinterestLink = value; }
        }
        public string GooglePlusLink
        {
            get { return Record.GooglePlusLink; }
            set { Record.GooglePlusLink = value; }
        }
    }
}