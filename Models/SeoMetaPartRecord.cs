﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class SeoMetaPartRecord : ContentPartRecord
    {
        public virtual string Description { get; set; }
        public virtual string Keywords { get; set; }       
        public virtual string OgImage { get; set; }
        public virtual string OtherHeader { get; set; }
    }
}