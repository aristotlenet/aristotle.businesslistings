﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class MarinaListingPartRecord : ContentPartRecord
    {
        //many fields come from the attached BusinessListingPart
        //amenities and categories from taxonomies

        public virtual string Hours { get; set; }
    }
}