﻿using System;
using Orchard.ContentManagement.Records;


namespace Aristotle.BusinessListings.Models
{
    public class AttractionListingPartRecord : ContentPartRecord
    {
        //many fields come from the attached BusinessListingPart
        // attraction amenities and categories from taxonomies

        public virtual string Hours { get; set; }
       
    }
}