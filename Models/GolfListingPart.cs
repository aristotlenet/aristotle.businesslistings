﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class GolfListingPart : ContentPart<GolfListingPartRecord>
    {
        public int Holes
        {
            get { return Record.Holes; }
            set { Record.Holes = value; }
        }
        public int Par
        {
            get { return Record.Par; }
            set { Record.Par = value; }
        }
        public int Yards
        {
            get { return Record.Yards; }
            set { Record.Yards = value; }
        }
    }
}