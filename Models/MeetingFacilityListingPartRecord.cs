﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class MeetingFacilityListingPartRecord : ContentPartRecord
    {
        public virtual int GuestRooms { get; set; }
        public virtual int MeetingRooms { get; set; }
        public virtual int MeetingSpace { get; set; }
        public virtual int TheaterCapacity { get; set; }
        public virtual int BanquetCapacity { get; set; }
        public virtual int BoothCount { get; set; }
        public virtual string FacilityNotes { get; set; }

    }
}