﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class MeetingFacilityListingPart : ContentPart<MeetingFacilityListingPartRecord>
    {
        public int GuestRooms
        {
            get { return Record.GuestRooms; }
            set { Record.GuestRooms = value; }
        }
        public int MeetingRooms
        {
            get { return Record.MeetingRooms; }
            set { Record.MeetingRooms = value; }
        }
        public int MeetingSpace
        {
            get { return Record.MeetingSpace; }
            set { Record.MeetingSpace = value; }
        }
        public int TheaterCapacity
        {
            get { return Record.TheaterCapacity; }
            set { Record.TheaterCapacity = value; }
        }
        public int BanquetCapacity
        {
            get { return Record.BanquetCapacity; }
            set { Record.BanquetCapacity = value; }
        }
        public int BoothCount
        {
            get { return Record.BoothCount; }
            set { Record.BoothCount = value; }
        }

        public string FacilityNotes
        {
            get { return Record.FacilityNotes; }
            set { Record.FacilityNotes = value; }
        }
    }
}