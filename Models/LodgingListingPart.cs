﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Aristotle.BusinessListings.Models
{
    public class LodgingListingPart : ContentPart<LodgingListingPartRecord>
    {
        public int NumberOfRooms
        {
            get { return Record.NumberOfRooms; }
            set { Record.NumberOfRooms = value; }
        }
    }
}