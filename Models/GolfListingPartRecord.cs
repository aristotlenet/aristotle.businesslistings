﻿using System;
using Orchard.ContentManagement.Records;

namespace Aristotle.BusinessListings.Models
{
    public class GolfListingPartRecord : ContentPartRecord
    {
        //many fields come from the attached BusinessListingPart
        // golf course amenities and categories from taxonomies

        public virtual int Holes { get; set; }
        public virtual int Par { get; set; }
        public virtual int Yards { get; set; }
    }
}