﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aristotle.BusinessListings.ViewModels
{
    public class CityViewModel
    {
        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }
        public virtual int Zoom { get; set; }

        public bool HasCoordinates
        {
            get
            {
                return !(string.IsNullOrEmpty(GpsLat) || GpsLat == "0"
                    || string.IsNullOrEmpty(GpsLng) || GpsLng == "0");
            }
        }

    }
}