﻿using System;


namespace Aristotle.BusinessListings.ViewModels
{
    public class BusinessListingViewModel
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string TollFree { get; set; }
        public string Fax { get; set; }
        public string BookingUrl { get; set; }

        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public string GpsLat { get; set; }
        public string GpsLng { get; set; }

        public bool Featured { get; set; }
        public string CvbId { get; set; }

        public bool HasCoordinates
        {
            get
            {
                return !(string.IsNullOrEmpty(GpsLat) || GpsLat == "0"
                    || string.IsNullOrEmpty(GpsLng) || GpsLng == "0");
            }
        }

        public string CityStateZip
        {
            get
            {
                return string.Format("{0}, {1} {2}", City, State, ZipCode).Trim(' ', ',');
            }
        }

        public string GeocodeAddress
        {
            get
            {
                return string.Format("{0}, {1}, {2} {3}", Address, City, State, ZipCode);
            }
        }
    }
}