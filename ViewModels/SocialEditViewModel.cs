﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aristotle.BusinessListings.ViewModels
{
    public class SocialEditViewModel
    {
        public virtual string FacebookLink { get; set; }
        public virtual string TwitterLink { get; set; }
        public virtual string PinterestLink { get; set; }
        public virtual string GooglePlusLink { get; set; }
    }
}