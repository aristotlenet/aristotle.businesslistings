﻿using System.ComponentModel.DataAnnotations;
using Orchard.Core.Common.ViewModels;

namespace Aristotle.BusinessListings.ViewModels
{
    public class BusinessListingEditViewModel
    {
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }
        public virtual string TollFree { get; set; }
        public virtual string Fax { get; set; }
        public virtual string BookingUrl { get; set; }

        public virtual string Address { get; set; }
        public virtual string Address2 { get; set; }
        [Required]
        public virtual string State { get; set; }
        [Required]
        public virtual string ZipCode { get; set; }
        public virtual int CityId { get; set; }

       
        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }

        public virtual bool Featured { get; set; }
        public virtual string CvbId { get; set; }

    }
}