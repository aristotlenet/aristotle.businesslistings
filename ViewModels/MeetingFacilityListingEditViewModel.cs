﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aristotle.BusinessListings.ViewModels
{
    public class MeetingFacilityListingEditViewModel
    {
        public virtual int GuestRooms { get; set; }
        public virtual int MeetingRooms { get; set; }
        public virtual int MeetingSpace { get; set; }
        public virtual int TheaterCapacity { get; set; }
        public virtual int BanquetCapacity { get; set; }
        public virtual int BoothCount { get; set; }
        public virtual string FacilityNotes { get; set; }

    }
}