﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aristotle.BusinessListings.ViewModels
{
    public class SeoMetaEditViewModel
    {
        public virtual string Description { get; set; }
        public virtual string Keywords { get; set; }
        public virtual string FacebookLink { get; set; }
        public virtual string TwitterLink { get; set; }
        public virtual string PinterestLink { get; set; }
        public virtual string GooglePlusLink { get; set; }
        public virtual string OgImage { get; set; }
        public virtual string OtherHeader { get; set; }
    }
}