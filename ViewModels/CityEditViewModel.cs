﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aristotle.BusinessListings.ViewModels
{
    public class CityEditViewModel
    {
        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }
        public virtual int Zoom { get; set; }
    }
}