﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aristotle.BusinessListings.ViewModels
{
    public class GolfListingEditViewModel
    {
        public virtual int Holes { get; set; }
        public virtual int Par { get; set; }
        public virtual int Yards { get; set; }
    }
}