﻿using Orchard.UI.Resources;

namespace Aristotle.BusinessListings {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();

            manifest.DefineStyle("BusinessListings_Edit").SetUrl("aristotle-businessListings-edit.css");
            manifest.DefineStyle("BusinessListings_Detail").SetUrl("aristotle-businessListings-detail.css");
            manifest.DefineScript("GoogleMaps").SetUrl("http://maps.google.com/maps/api/js?sensor=false");
            manifest.DefineScript("jQueryAutoGeocode").SetUrl("jquery.AutoGeocode.js").SetDependencies("jQuery");
        }
    }
}