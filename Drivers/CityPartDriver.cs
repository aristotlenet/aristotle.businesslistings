﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class CityPartDriver : ContentPartDriver<CityPart>
    {
     
        protected override string Prefix {
            get { return "Aristotle.BusinessListings.CityPart"; }
        }


        public CityPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Display(CityPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_City_Detail",
                () => {
                    var viewModel = new CityViewModel {
                      
                        GpsLat = part.GpsLat,
                        GpsLng = part.GpsLng,
                        Zoom = part.Zoom,
                        
                    };
                    return shapeHelper.Parts_City_Detail(Model: viewModel);
                });
        }

        protected override DriverResult Editor(CityPart part, dynamic shapeHelper)
        {
         
            var model = new CityEditViewModel{
               
               
                GpsLat = part.GpsLat,
                GpsLng = part.GpsLng,
                Zoom = part.Zoom,
                
            };
            return ContentShape("Parts_City_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.CityPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(CityPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new CityEditViewModel();
            
            updater.TryUpdateModel(model, Prefix, null, null);

            part.GpsLat = model.GpsLat;
            part.GpsLng = model.GpsLng;
            part.Zoom = model.Zoom;
          
            return Editor(part, shapeHelper);
        }

        protected override void Importing(CityPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

           
            context.ImportAttribute(partName, "GpsLat", value => part.GpsLat = value);
            context.ImportAttribute(partName, "GpsLng", value => part.GpsLng = value);
            context.ImportAttribute(partName, "Zoom", value => part.Zoom = Convert.ToInt32(value));
           

            }

        protected override void Exporting(CityPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("GpsLat", part.GpsLat);
            element.SetAttributeValue("GpsLng", part.GpsLng);
            element.SetAttributeValue("Zoom", part.Zoom);
           
        }

       
    }
}