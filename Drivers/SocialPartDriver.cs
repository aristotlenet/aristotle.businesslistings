﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;


namespace Aristotle.BusinessListings.Drivers
{
    public class SocialPartDriver: ContentPartDriver<SocialPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.SocialPart"; }
        }

        public SocialPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(SocialPart part, dynamic shapeHelper)
        {
            var model = new SocialEditViewModel
            {

                FacebookLink = part.FacebookLink,
                GooglePlusLink = part.GooglePlusLink,
                TwitterLink = part.TwitterLink,
                PinterestLink = part.PinterestLink,
              
            };
            return ContentShape("Parts_Social_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.SocialPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(SocialPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new SocialEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.FacebookLink = model.FacebookLink;
            part.GooglePlusLink = model.GooglePlusLink;

            part.TwitterLink = model.TwitterLink;
            part.PinterestLink = model.PinterestLink;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(SocialPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_Social_Detail",
                () => {
                    var viewModel = new SocialViewModel {
                        FacebookLink = part.FacebookLink,
                        GooglePlusLink = part.GooglePlusLink,
                        TwitterLink = part.TwitterLink,
                        PinterestLink = part.PinterestLink,

                    };
                    return shapeHelper.Parts_Social_Detail(Model: viewModel);
                });

        }

        protected override void Importing(SocialPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "FacebookLink", value => part.FacebookLink = value);
            context.ImportAttribute(partName, "GooglePlusLink", value => part.GooglePlusLink = value);

            context.ImportAttribute(partName, "TwitterLink", value => part.TwitterLink = value);
            context.ImportAttribute(partName, "PinterestLink", value => part.PinterestLink = value);
           

        }

        protected override void Exporting(SocialPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("FacebookLink", part.FacebookLink);
            element.SetAttributeValue("GooglePlusLink", part.GooglePlusLink);

            element.SetAttributeValue("TwitterLink", part.TwitterLink);
            element.SetAttributeValue("PinterestLink", part.PinterestLink);
           
        }
        
    }
   
}