﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class LodgingListingPartDriver : ContentPartDriver<LodgingListingPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.LodgingListingPart"; }
        }

        public LodgingListingPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(LodgingListingPart part, dynamic shapeHelper)
        {
            var model = new LodgingListingEditViewModel
            {

                NumberOfRooms = part.NumberOfRooms,
              
            };
            return ContentShape("Parts_LodgingListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.LodgingListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(LodgingListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new LodgingListingEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.NumberOfRooms = model.NumberOfRooms;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(LodgingListingPart part, string displayType, dynamic shapeHelper) {

            return ContentShape("Parts_LodgingListing_Detail",
                () => {
                    var viewModel = new LodgingListingViewModel {
                        NumberOfRooms = part.NumberOfRooms,

                    };
                    return shapeHelper.Parts_LodgingListing_Detail(Model: viewModel);
                });
        }

        protected override void Importing(LodgingListingPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "NumberOfRooms", value => part.NumberOfRooms = Convert.ToInt32(value));
          

        }

        protected override void Exporting(LodgingListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("NumberOfRooms", part.NumberOfRooms);
            
        }
        
    }
   
}