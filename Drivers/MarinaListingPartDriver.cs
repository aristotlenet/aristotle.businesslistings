﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class MarinaListingPartDriver: ContentPartDriver<MarinaListingPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.MarinaListingPart"; }
        }

        public MarinaListingPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(MarinaListingPart part, dynamic shapeHelper)
        {
            var model = new MarinaListingEditViewModel
            {

                Hours = part.Hours,
              
            };
            return ContentShape("Parts_MarinaListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.MarinaListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(MarinaListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new MarinaListingEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.Hours = model.Hours;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(MarinaListingPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_MarinaListing_Detail",
                () => {
                    var viewModel = new MarinaListingViewModel {
                        Hours = part.Hours
                    };
                    return shapeHelper.Parts_MarinaListing_Detail(Model: viewModel);
                });

        }

        protected override void Importing(MarinaListingPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "Hours", value => part.Hours = value);
       

        }

        protected override void Exporting(MarinaListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("Hours", part.Hours);
           
        }
        
    }
}