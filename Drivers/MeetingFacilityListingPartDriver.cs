﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class MeetingFacilityListingPartDriver : ContentPartDriver<MeetingFacilityListingPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.MeetingFacilityListingPart"; }
        }

        public MeetingFacilityListingPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(MeetingFacilityListingPart part, dynamic shapeHelper)
        {
            var model = new MeetingFacilityListingEditViewModel
            {

                GuestRooms = part.GuestRooms,
                MeetingRooms = part.MeetingRooms,
                MeetingSpace = part.MeetingSpace,
                TheaterCapacity = part.TheaterCapacity,
                BanquetCapacity = part.BanquetCapacity,
                BoothCount = part.BoothCount,
                FacilityNotes = part.FacilityNotes,
              
            };
            return ContentShape("Parts_MeetingFacilityListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.MeetingFacilityListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(MeetingFacilityListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new MeetingFacilityListingEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.GuestRooms = model.GuestRooms;
            part.MeetingRooms = model.MeetingRooms;
            part.MeetingSpace = model.MeetingSpace;
            part.TheaterCapacity = model.TheaterCapacity;
            part.BanquetCapacity = model.BanquetCapacity;
            part.BoothCount = model.BoothCount;
            part.FacilityNotes = model.FacilityNotes;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(MeetingFacilityListingPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_MeetingFacilityListing_Detail",
                () => {
                    var viewModel = new MeetingFacilityListingViewModel {
                        GuestRooms = part.GuestRooms,
                        MeetingRooms = part.MeetingRooms,
                        MeetingSpace = part.MeetingSpace,
                        TheaterCapacity = part.TheaterCapacity,
                        BanquetCapacity = part.BanquetCapacity,
                        BoothCount = part.BoothCount,
                        FacilityNotes = part.FacilityNotes,

                    };
                    return shapeHelper.Parts_MeetingFacilityListing_Detail(Model: viewModel);
                });
        }

        protected override void Importing(MeetingFacilityListingPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "GuestRooms", value => part.GuestRooms = Convert.ToInt32(value));
            context.ImportAttribute(partName, "MeetingRooms", value => part.MeetingRooms = Convert.ToInt32(value));
            context.ImportAttribute(partName, "MeetingSpace", value => part.MeetingSpace = Convert.ToInt32(value));
            context.ImportAttribute(partName, "TheaterCapacity", value => part.TheaterCapacity = Convert.ToInt32(value));
            context.ImportAttribute(partName, "BanquetCapacity", value => part.BanquetCapacity = Convert.ToInt32(value));
            context.ImportAttribute(partName, "BoothCount", value => part.BoothCount = Convert.ToInt32(value));
            context.ImportAttribute(partName, "FacilityNotes", value => part.FacilityNotes = value);
          

        }

        protected override void Exporting(MeetingFacilityListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("GuestRooms", part.GuestRooms);
            element.SetAttributeValue("MeetingRooms", part.MeetingRooms);
            element.SetAttributeValue("MeetingSpace", part.MeetingSpace);
            element.SetAttributeValue("TheaterCapacity", part.TheaterCapacity);
            element.SetAttributeValue("BanquetCapacity", part.BanquetCapacity);
            element.SetAttributeValue("BoothCount", part.BoothCount);
            element.SetAttributeValue("FacilityNotes", part.FacilityNotes);
          
        }
        
    }
   
}