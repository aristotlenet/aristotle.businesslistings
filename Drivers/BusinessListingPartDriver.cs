﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;

namespace Aristotle.BusinessListings.Drivers
{
    public class BusinessListingPartDriver : ContentPartDriver<BusinessListingPart>
    {
     
        protected override string Prefix {
            get { return "Aristotle.BusinessListings.BusinessListingPart"; }
        }


        public BusinessListingPartDriver(
            IOrchardServices services,
            IRepository<CityPartRecord> repository)
        {          
            Services = services;
            Cities = repository;
        }

        public IOrchardServices Services { get; set; }
        public IRepository<CityPartRecord> Cities { get; set; }

        protected override DriverResult Display(BusinessListingPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_BusinessListing_Detail",
                () => {
                    var viewModel = new BusinessListingViewModel {
                        Phone = part.Phone,
                        Email = part.Email,
                        Website = part.Website,
                        TollFree = part.TollFree,
                        Fax = part.Fax,
                        BookingUrl = part.BookingUrl,
                        Address = part.Address,
                        Address2 = part.Address2,
                        State = part.State,
                        ZipCode = part.ZipCode,
                        GpsLat = part.GpsLat,
                        GpsLng = part.GpsLng,
                        CvbId = part.CvbId,
                        Featured = part.Featured,
                        City = part.City.Name
                    };
                    return shapeHelper.Parts_BusinessListing_Detail(Model: viewModel);
                });
        }

        protected override DriverResult Editor(BusinessListingPart part, dynamic shapeHelper)
        {
         
            var model = new BusinessListingEditViewModel{
               
                Phone = part.Phone,
                Email = part.Email,
                Website = part.Website,
                TollFree = part.TollFree,
                Fax = part.Fax,
                BookingUrl = part.BookingUrl,
                Address = part.Address,
                Address2 = part.Address2,
                State = part.State,
                ZipCode = part.ZipCode,
                GpsLat = part.GpsLat,
                GpsLng = part.GpsLng,
                Featured = part.Featured,
                CvbId = part.CvbId ,
                CityId = part.City.ContentItemRecord.Id
            };
            return ContentShape("Parts_BusinessListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.BusinessListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(BusinessListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new BusinessListingEditViewModel();
            
            updater.TryUpdateModel(model, Prefix, null, null);


            part.Phone = model.Phone;
            part.Email = model.Email;
            part.Website = model.Website;
            part.TollFree = model.TollFree;
            part.Fax = model.Fax;
            part.BookingUrl = model.BookingUrl;

            part.Address = model.Address;
            part.Address2 = model.Address2;
            part.City = Cities.Get(T => T.ContentItemRecord.Id == model.CityId);
            part.State = model.State;
            part.ZipCode = model.ZipCode;

            part.GpsLat = model.GpsLat;
            part.GpsLng = model.GpsLng;
            part.Featured = model.Featured;
            part.CvbId = model.CvbId;

            return Editor(part, shapeHelper);
        }

        protected override void Importing(BusinessListingPart part, ImportContentContext context) {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "Phone", value => part.Phone = value);
            context.ImportAttribute(partName, "Email", value => part.Email = value);
            context.ImportAttribute(partName, "Website", value => part.Website = value);
            context.ImportAttribute(partName, "TollFree", value => part.TollFree = value);
            context.ImportAttribute(partName, "Fax", value => part.Fax = value);
            context.ImportAttribute(partName, "BookingUrl", value => part.BookingUrl = value);
            context.ImportAttribute(partName, "Address", value => part.Address = value);
            context.ImportAttribute(partName, "Address2", value => part.Address2 = value);
            context.ImportAttribute(partName, "City", value => part.City = Cities.Get(T => T.ContentItemRecord.Id == Convert.ToInt32(value)));
            context.ImportAttribute(partName, "State", value => part.State = value);
            context.ImportAttribute(partName, "ZipCode", value => part.ZipCode = value);
            context.ImportAttribute(partName, "GpsLat", value => part.GpsLat = value);
            context.ImportAttribute(partName, "GpsLng", value => part.GpsLng = value);
            context.ImportAttribute(partName, "Featured", value => part.Featured = Convert.ToBoolean(value));
            context.ImportAttribute(partName, "CvbId", value => part.CvbId = value);

            }

        protected override void Exporting(BusinessListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("Phone", part.Phone);
            element.SetAttributeValue("Email", part.Email);
            element.SetAttributeValue("Website", part.Website);
            element.SetAttributeValue("TollFree", part.TollFree);
            element.SetAttributeValue("Fax", part.Fax);
            element.SetAttributeValue("BookingUrl", part.BookingUrl);
            element.SetAttributeValue("Address", part.Address);
            element.SetAttributeValue("Address2", part.Address2);
            element.SetAttributeValue("State", part.State);
            element.SetAttributeValue("ZipCode", part.ZipCode);
            element.SetAttributeValue("GpsLat", part.GpsLat);
            element.SetAttributeValue("GpsLng", part.GpsLng);
            element.SetAttributeValue("Featured", part.Featured);
            element.SetAttributeValue("CvbId", part.CvbId);
            element.SetAttributeValue("City", part.City.Name);
        }

       
    }
}