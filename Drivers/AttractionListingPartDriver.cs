﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class AttractionListingPartDriver : ContentPartDriver<AttractionListingPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.AttractionListingPart"; }
        }

        public AttractionListingPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(AttractionListingPart part, dynamic shapeHelper)
        {
            var model = new AttractionListingEditViewModel
            {

                Hours = part.Hours,
              
            };
            return ContentShape("Parts_AttractionListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.AttractionListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(AttractionListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new AttractionListingEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.Hours = model.Hours;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(AttractionListingPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_AttractionListing_Detail",
                () => {
                    var viewModel = new AttractionListingViewModel {
                        Hours = part.Hours
                    };
                    return shapeHelper.Parts_AttractionListing_Detail(Model: viewModel);
                });

        }

        protected override void Importing(AttractionListingPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "Hours", value => part.Hours = value);
       

        }

        protected override void Exporting(AttractionListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("Hours", part.Hours);
           
        }
        
    }
}