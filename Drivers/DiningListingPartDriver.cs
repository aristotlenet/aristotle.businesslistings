﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
namespace Aristotle.BusinessListings.Drivers
{
    public class DiningListingPartDriver : ContentPartDriver<DiningListingPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.DiningListingPart"; }
        }

        public DiningListingPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(DiningListingPart part, dynamic shapeHelper)
        {
            var model = new DiningListingEditViewModel
            {

                SeatingCapacity = part.SeatingCapacity,
              
            };
            return ContentShape("Parts_DiningListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.DiningListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(DiningListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new DiningListingEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.SeatingCapacity = model.SeatingCapacity;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(DiningListingPart part, string displayType, dynamic shapeHelper)
        {
            return
                 ContentShape("Parts_DiningListing_Detail",
                    () =>
                    {
                        var viewModel = new DiningListingViewModel
                        {
                            SeatingCapacity = part.SeatingCapacity,
                           
                        };
                        return shapeHelper.Parts_DiningListing_Detail(Model: viewModel);
                    })
            ;
        }

        protected override void Importing(DiningListingPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "SeatingCapacity", value => part.SeatingCapacity = Convert.ToInt32(value));
           

        }

        protected override void Exporting(DiningListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("SeatingCapacity", part.SeatingCapacity);
          
        }
        
    }
  
}