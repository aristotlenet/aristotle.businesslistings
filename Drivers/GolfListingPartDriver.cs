﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class GolfListingPartDriver : ContentPartDriver<GolfListingPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.GolfListingPart"; }
        }

        public GolfListingPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(GolfListingPart part, dynamic shapeHelper)
        {
            var model = new GolfListingEditViewModel
            {

                Holes = part.Holes,
                Yards = part.Yards,
                Par = part.Par,
              
            };
            return ContentShape("Parts_GolfListing_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.GolfListingPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(GolfListingPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new GolfListingEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.Holes = model.Holes;
            part.Yards = model.Yards;
            part.Par = model.Par;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(GolfListingPart part, string displayType, dynamic shapeHelper) {
            return
                ContentShape("Parts_GolfListing_Detail",
                    () => {
                        var viewModel = new GolfListingViewModel {
                            Holes = part.Holes,
                            Par = part.Par,
                            Yards = part.Yards
                        };
                        return shapeHelper.Parts_GolfListing_Detail(Model: viewModel);
                    })
                ;
        }

        protected override void Importing(GolfListingPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "Holes", value => part.Holes = Convert.ToInt32(value));
            context.ImportAttribute(partName, "Par", value => part.Par = Convert.ToInt32(value));
            context.ImportAttribute(partName, "Yards", value => part.Yards = Convert.ToInt32(value));
          

        }

        protected override void Exporting(GolfListingPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("Holes", part.Holes);
            element.SetAttributeValue("Par", part.Par);
            element.SetAttributeValue("Yards", part.Yards);
           
        }
        
    }
  
}