﻿using Orchard;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;
using Aristotle.BusinessListings.Models;
using Orchard.Taxonomies;
using Orchard.Taxonomies.Services;

namespace Aristotle.BusinessListings.Drivers {

    public class BusinessListingSearchPartDriver : ContentPartDriver<BusinessListingSearchPart> {
        public BusinessListingSearchPartDriver(
            ITaxonomyService taxonomy) {
            Taxonomy = taxonomy;
        }

        public ITaxonomyService Taxonomy { get; set; }

        protected override string Prefix {
            get { return "Aristotle.BusinessListings.BusinessListingSearchPart"; }
        }

        protected override DriverResult Display(BusinessListingSearchPart part, string displayType, dynamic shapeHelper) {
            var terms = Taxonomy.GetTaxonomyByName("Business Category").Terms;

            return ContentShape("Parts_BusinessListingSearch",
                () => shapeHelper.Parts_BusinessListingSearch(Terms: terms));
        }
    }

}