﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;


namespace Aristotle.BusinessListings.Drivers
{
    public class ContactPartDriver : ContentPartDriver<ContactPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.ContactPart"; }
        }

        public ContactPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(ContactPart part, dynamic shapeHelper)
        {
            var model = new ContactEditViewModel
            {

                FirstName = part.FirstName,
                LastName = part.LastName,
                Company = part.Company,
                Phone = part.Phone,
                Email = part.Email,
                Address = part.Address,
                Address2 = part.Address2,
                City = part.City,
                State = part.State,
                ZipCode = part.ZipCode,
               
            };
            return ContentShape("Parts_Contact_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.ContactPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(ContactPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new ContactEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.FirstName = model.FirstName;
            part.LastName = model.LastName;
            part.Company = model.Company;
            part.Phone = model.Phone;
            part.Email = model.Email;
            part.Address = model.Address;
            part.Address2 = model.Address2;
            part.City = model.City;
            part.State = model.State;
            part.ZipCode = model.ZipCode;
          
            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(ContactPart part, string displayType, dynamic shapeHelper) {
            // we will probably not display this in real life
            return
                ContentShape("Parts_Contact_Detail",
                    () => {
                        var viewModel = new ContactViewModel {
                            FirstName = part.FirstName,
                            LastName = part.LastName,
                            Company = part.Company,
                            Phone = part.Phone,
                            Email = part.Email,
                            Address = part.Address,
                            Address2 = part.Address2,
                            City = part.City,
                            State = part.State,
                            ZipCode = part.ZipCode,
                        };
                        return shapeHelper.Parts_Contact_Detail(Model: viewModel);
                    });
        }

        protected override void Importing(ContactPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;
            context.ImportAttribute(partName, "FirstName", value => part.FirstName = value);
            context.ImportAttribute(partName, "LastName", value => part.LastName = value);
            context.ImportAttribute(partName, "Company", value => part.Company = value);           
            context.ImportAttribute(partName, "Phone", value => part.Phone = value);
            context.ImportAttribute(partName, "Email", value => part.Email = value);
            context.ImportAttribute(partName, "Address", value => part.Address = value);
            context.ImportAttribute(partName, "Address2", value => part.Address2 = value);
            context.ImportAttribute(partName, "City", value => part.City = value);
            context.ImportAttribute(partName, "State", value => part.State = value);
            context.ImportAttribute(partName, "ZipCode", value => part.ZipCode = value);
        }

        protected override void Exporting(ContactPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("FirstName", part.FirstName);
            element.SetAttributeValue("LastName", part.LastName);
            element.SetAttributeValue("Phone", part.Phone);
            element.SetAttributeValue("Email", part.Email);
            element.SetAttributeValue("Company", part.Company);          
            element.SetAttributeValue("Address", part.Address);
            element.SetAttributeValue("Address2", part.Address2);
            element.SetAttributeValue("City", part.City);
            element.SetAttributeValue("State", part.State);
            element.SetAttributeValue("ZipCode", part.ZipCode);
            
        }
        
    }
 
}