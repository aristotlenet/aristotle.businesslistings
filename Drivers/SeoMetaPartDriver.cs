﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

namespace Aristotle.BusinessListings.Drivers
{
    public class SeoMetaPartDriver : ContentPartDriver<SeoMetaPart>
    {
        protected override string Prefix
        {
            get { return "Aristotle.BusinessListings.SeoMetaPart"; }
        }

        public SeoMetaPartDriver(
            IOrchardServices services) {          
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Editor(SeoMetaPart part, dynamic shapeHelper)
        {
            var model = new SeoMetaEditViewModel
            {

                Description = part.Description,
                Keywords = part.Keywords,
              
                OgImage = part.OgImage,
                OtherHeader = part.OtherHeader,
              
            };
            return ContentShape("Parts_SeoMeta_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.SeoMetaPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(SeoMetaPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var model = new SeoMetaEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);

            part.Description = model.Description;
            part.Keywords = model.Keywords;
         
            part.OgImage = model.OgImage;
            part.OtherHeader = model.OtherHeader;

            return Editor(part, shapeHelper);
        }


        protected override DriverResult Display(SeoMetaPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_SeoMeta_Detail",
                () => {
                    var viewModel = new SeoMetaViewModel {
                        Keywords = part.Keywords,
                        Description = part.Description,

                        OgImage = part.OgImage,
                        OtherHeader = part.OtherHeader,

                    };
                    return shapeHelper.Parts_SeoMeta_Detail(Model: viewModel);
                });

        }

        protected override void Importing(SeoMetaPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "Keywords", value => part.Keywords = value);
            context.ImportAttribute(partName, "Description", value => part.Description = value);
          
            context.ImportAttribute(partName, "OgImage", value => part.OgImage = value);
            context.ImportAttribute(partName, "OtherHeader", value => part.OtherHeader = value);
           

        }

        protected override void Exporting(SeoMetaPart part, ExportContentContext context)
        {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("Keywords", part.Keywords);
            element.SetAttributeValue("Description", part.Description);
           
            element.SetAttributeValue("OgImage", part.OgImage);
            element.SetAttributeValue("OtherHeader", part.OtherHeader);
           
        }
        
    }
   
}