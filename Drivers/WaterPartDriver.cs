﻿using System;
using Aristotle.BusinessListings.Models;
using Aristotle.BusinessListings.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;


namespace Aristotle.BusinessListings.Drivers {
    public class WaterPartDriver : ContentPartDriver<WaterPart> {

        protected override string Prefix {
            get { return "Aristotle.BusinessListings.WaterPart"; }
        }


        public WaterPartDriver(
            IOrchardServices services,
            IRepository<CityPartRecord> repository)
        {
            Services = services;
            Cities = repository;
        }

        public IOrchardServices Services { get; set; }
        public IRepository<CityPartRecord> Cities { get; set; }

        protected override DriverResult Display(WaterPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_Water_Detail",
                () => {
                    var viewModel = new WaterViewModel {
                        Phone = part.Phone,
                        Email = part.Email,
                        Website = part.Website,
                        TollFree = part.TollFree,
                        Fax = part.Fax,
                        Address = part.Address,
                        Address2 = part.Address2,
                        State = part.State,
                        ZipCode = part.ZipCode,
                        City = part.City.Name
                    };
                    return shapeHelper.Parts_Water_Detail(Model: viewModel);
                });
        }

        protected override DriverResult Editor(WaterPart part, dynamic shapeHelper) {

            var model = new WaterEditViewModel {

                Phone = part.Phone,
                Email = part.Email,
                Website = part.Website,
                TollFree = part.TollFree,
                Fax = part.Fax,
                Address = part.Address,
                Address2 = part.Address2,
                State = part.State,
                ZipCode = part.ZipCode,
                CityId = part.City.ContentItemRecord.Id
            };
            return ContentShape("Parts_Water_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.WaterPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(WaterPart part, IUpdateModel updater, dynamic shapeHelper) {
            var model = new WaterEditViewModel();

            updater.TryUpdateModel(model, Prefix, null, null);


            part.Phone = model.Phone;
            part.Email = model.Email;
            part.Website = model.Website;
            part.TollFree = model.TollFree;
            part.Fax = model.Fax;

            part.Address = model.Address;
            part.Address2 = model.Address2;
            part.City = Cities.Get(T => T.ContentItemRecord.Id ==  model.CityId);
            part.State = model.State;
            part.ZipCode = model.ZipCode;

            return Editor(part, shapeHelper);
        }

        protected override void Importing(WaterPart part, ImportContentContext context) {
            var partName = part.PartDefinition.Name;

            context.ImportAttribute(partName, "Phone", value => part.Phone = value);
            context.ImportAttribute(partName, "Email", value => part.Email = value);
            context.ImportAttribute(partName, "Website", value => part.Website = value);
            context.ImportAttribute(partName, "TollFree", value => part.TollFree = value);
            context.ImportAttribute(partName, "Fax", value => part.Fax = value);
            context.ImportAttribute(partName, "Address", value => part.Address = value);
            context.ImportAttribute(partName, "Address2", value => part.Address2 = value);
            context.ImportAttribute(partName, "City", value => part.City = Cities.Get(T => T.ContentItemRecord.Id == Convert.ToInt32(value)));
            context.ImportAttribute(partName, "State", value => part.State = value);
            context.ImportAttribute(partName, "ZipCode", value => part.ZipCode = value);

        }

        protected override void Exporting(WaterPart part, ExportContentContext context) {
            var element = context.Element(part.PartDefinition.Name);

            element.SetAttributeValue("Phone", part.Phone);
            element.SetAttributeValue("Email", part.Email);
            element.SetAttributeValue("Website", part.Website);
            element.SetAttributeValue("TollFree", part.TollFree);
            element.SetAttributeValue("Fax", part.Fax);
            element.SetAttributeValue("Address", part.Address);
            element.SetAttributeValue("Address2", part.Address2);
            element.SetAttributeValue("City", part.City.Name);
            element.SetAttributeValue("State", part.State);
            element.SetAttributeValue("ZipCode", part.ZipCode);

        }

    }
}