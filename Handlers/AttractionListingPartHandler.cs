﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;

namespace Aristotle.BusinessListings.Handlers
{
    public class AttractionListingPartHandler : ContentHandler
    {
        public AttractionListingPartHandler(
            IRepository<AttractionListingPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}