﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;

namespace Aristotle.BusinessListings.Handlers
{
    public class BusinessListingPartHandler : ContentHandler
    {
        public BusinessListingPartHandler(
            IRepository<BusinessListingPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}