﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;

namespace Aristotle.BusinessListings.Handlers
{
    public class MarinaListingPartHandler : ContentHandler
    {
        public MarinaListingPartHandler(
            IRepository<MarinaListingPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}