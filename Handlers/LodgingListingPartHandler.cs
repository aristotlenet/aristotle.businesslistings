﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;


namespace Aristotle.BusinessListings.Handlers
{
    public class LodgingListingPartHandler : ContentHandler
    {
        public LodgingListingPartHandler(
            IRepository<LodgingListingPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}