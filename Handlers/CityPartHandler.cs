﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;

namespace Aristotle.BusinessListings.Handlers
{
    public class CityPartHandler : ContentHandler
    {
        public CityPartHandler(
            IRepository<CityPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}