﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;


namespace Aristotle.BusinessListings.Handlers
{
    public class MeetingFacilityListingPartHandler : ContentHandler
    {
        public MeetingFacilityListingPartHandler(
            IRepository<MeetingFacilityListingPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}