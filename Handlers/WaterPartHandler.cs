﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;

namespace Aristotle.BusinessListings.Handlers
{
    public class WaterPartHandler : ContentHandler
    {
        public WaterPartHandler(
            IRepository<WaterPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}