﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;

namespace Aristotle.BusinessListings.Handlers
{
    public class SocialPartHandler : ContentHandler
    {
        public SocialPartHandler(
            IRepository<SocialPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}