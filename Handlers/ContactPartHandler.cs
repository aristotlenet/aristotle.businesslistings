﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;


namespace Aristotle.BusinessListings.Handlers
{
    public class ContactPartHandler : ContentHandler
    {
        public ContactPartHandler(
            IRepository<ContactPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}