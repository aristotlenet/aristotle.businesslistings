﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;


namespace Aristotle.BusinessListings.Handlers
{
    public class GolfListingPartHandler : ContentHandler
    {
        public GolfListingPartHandler(
            IRepository<GolfListingPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}