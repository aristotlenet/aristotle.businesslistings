﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;


namespace Aristotle.BusinessListings.Handlers
{
    public class DiningListingPartHandler : ContentHandler
    {
        public DiningListingPartHandler(
            IRepository<DiningListingPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}