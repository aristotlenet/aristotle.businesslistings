﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Aristotle.BusinessListings.Models;


namespace Aristotle.BusinessListings.Handlers
{
    public class SeoMetaPartHandler : ContentHandler
    {
        public SeoMetaPartHandler(
            IRepository<SeoMetaPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

        }
    }
}