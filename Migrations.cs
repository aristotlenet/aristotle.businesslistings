using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.ContentManagement;
using Orchard.Taxonomies.Models;
using Orchard.Indexing;
using Orchard.Environment.Extensions;

namespace Aristotle.BusinessListings {
    public class Migrations : DataMigrationImpl {

        private readonly IContentManager _contentManager;
        private readonly IIndexManager _indexManager;

        public Migrations(IContentManager contentManager, IIndexManager indexManager)
        {
            _contentManager = contentManager;
            _indexManager = indexManager;
        }

        public int Create() {
			// Creating table BusinessListingPartRecord
			SchemaBuilder.CreateTable("BusinessListingPartRecord", table => table
				.ContentPartRecord()
				.Column("Phone", DbType.String)
				.Column("TollFree", DbType.String)
				.Column("Fax", DbType.String)
				.Column("Email", DbType.String)
				.Column("Website", DbType.String)
				.Column("BookingUrl", DbType.String)
				.Column("Address", DbType.String)
				.Column("Address2", DbType.String)
				.Column("State", DbType.String)
				.Column("ZipCode", DbType.String)
				.Column("GpsLat", DbType.String)
				.Column("GpsLng", DbType.String)
				.Column("Featured", DbType.Boolean)
				.Column("CvbId", DbType.String)
				.Column("City_id", DbType.Int32)
			);

			// Creating table GolfListingPartRecord
			SchemaBuilder.CreateTable("GolfListingPartRecord", table => table
				.ContentPartRecord()
				.Column("Holes", DbType.Int32)
				.Column("Par", DbType.Int32)
				.Column("Yards", DbType.Int32)
			);

			// Creating table LodgingListingPartRecord
			SchemaBuilder.CreateTable("LodgingListingPartRecord", table => table
				.ContentPartRecord()
				.Column("NumberOfRooms", DbType.Int32)
			);

			// Creating table CityPartRecord
			SchemaBuilder.CreateTable("CityPartRecord", table => table
				.ContentPartRecord()
				.Column("Name", DbType.String)
				.Column("GpsLat", DbType.String)
				.Column("GpsLng", DbType.String)
				.Column("Zoom", DbType.Int32)
			);

			// Creating table MeetingFacilityListingPartRecord
			SchemaBuilder.CreateTable("MeetingFacilityListingPartRecord", table => table
				.ContentPartRecord()
				.Column("GuestRooms", DbType.Int32)
				.Column("MeetingRooms", DbType.Int32)
				.Column("MeetingSpace", DbType.Int32)
				.Column("TheaterCapacity", DbType.Int32)
				.Column("BanquetCapacity", DbType.Int32)
				.Column("BoothCount", DbType.Int32)
				.Column("FacilityNotes", DbType.String)
			);

			// Creating table SocialPartRecord
			SchemaBuilder.CreateTable("SocialPartRecord", table => table
				.ContentPartRecord()
				.Column("FacebookLink", DbType.String)
				.Column("TwitterLink", DbType.String)
				.Column("PinterestLink", DbType.String)
				.Column("GooglePlusLink", DbType.String)
			);

			// Creating table MarinaListingPartRecord
			SchemaBuilder.CreateTable("MarinaListingPartRecord", table => table
				.ContentPartRecord()
				.Column("Hours", DbType.String)
			);

			// Creating table ContactPartRecord
			SchemaBuilder.CreateTable("ContactPartRecord", table => table
				.ContentPartRecord()
				.Column("FirstName", DbType.String)
				.Column("LastName", DbType.String)
				.Column("Company", DbType.String)
				.Column("Address", DbType.String)
				.Column("Address2", DbType.String)
				.Column("City", DbType.String)
				.Column("State", DbType.String)
				.Column("ZipCode", DbType.String)
				.Column("Email", DbType.String)
				.Column("Phone", DbType.String)
			);

			// Creating table AttractionListingPartRecord
			SchemaBuilder.CreateTable("AttractionListingPartRecord", table => table
				.ContentPartRecord()
				.Column("Hours", DbType.String)
			);

			// Creating table DiningListingPartRecord
			SchemaBuilder.CreateTable("DiningListingPartRecord", table => table
				.ContentPartRecord()
				.Column("SeatingCapacity", DbType.Int32)
			);

			// Creating table WaterPartRecord
			SchemaBuilder.CreateTable("WaterPartRecord", table => table
				.ContentPartRecord()
				.Column("Phone", DbType.String)
				.Column("TollFree", DbType.String)
				.Column("Fax", DbType.String)
				.Column("Email", DbType.String)
				.Column("Website", DbType.String)
				.Column("Address", DbType.String)
				.Column("Address2", DbType.String)
				.Column("State", DbType.String)
				.Column("ZipCode", DbType.String)
				.Column("City_id", DbType.Int32)
			);

			// Creating table SeoMetaPartRecord
			SchemaBuilder.CreateTable("SeoMetaPartRecord", table => table
				.ContentPartRecord()
				.Column("Description", DbType.String)
				.Column("Keywords", DbType.String)
				.Column("OgImage", DbType.String)
				.Column("OtherHeader", DbType.String)
			);

            // create taxonomies
            var taxonomyContentItem = _contentManager.New("Taxonomy");
            var taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Region";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "City";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Attraction Category";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Lodging Category";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Dining Category";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Meeting Facility Category";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Attraction Amenities";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Lodging Amenities";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Dining Amenities";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Meeting Facility Amenities";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            taxonomyContentItem = _contentManager.New("Taxonomy");
            taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();
            taxonomyPart.Name = "Golf Amenities";
            taxonomyPart.IsInternal = true;
            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            // TODO: Fill out taxonomies

            //add taxonomies to parts

            ContentDefinitionManager.AlterPartDefinition("BusinessListingPart",
                cfg => cfg
                    .Attachable()
                    .WithField("Region",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Region")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Region")
                            .WithSetting("LeavesOnly", "true")
                            .WithSetting("Required", "true"))
                );
            ContentDefinitionManager.AlterPartDefinition("BusinessListingPart",
               cfg => cfg
                   .Attachable()
                   .WithField("City",
                       f => f
                           .OfType("TaxonomyField")
                           .WithDisplayName("City")
                           .WithSetting("TaxonomyFieldSettings.Taxonomy", "City")
                           .WithSetting("LeavesOnly", "true")
                           .WithSetting("Required", "true"))
               );
            ContentDefinitionManager.AlterPartDefinition("BusinessListingPart",
                builder => builder.WithField("Images",
                    fieldBuilder => fieldBuilder
                        .OfType("MediaLibraryPickerField")
                        .WithDisplayName("Images")));

            ContentDefinitionManager.AlterPartDefinition("AttractionListing",
                cfg => cfg
                    .Attachable()
                    .WithField("AttractionCategory",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Attraction Category")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Attraction Category")
                    )
                    .WithField("AttractionAmenities",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Attraction Amenities")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Attraction Amenities"))
                );

            ContentDefinitionManager.AlterPartDefinition("LodgingListing",
                cfg => cfg
                    .Attachable()
                    .WithField("LodgingCategory",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Lodging Category")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Lodging Category"))
                    .WithField("LodgingAmenities",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Lodging Amenities")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Lodging Amenities"))
                );

            ContentDefinitionManager.AlterPartDefinition("DiningListing",
                cfg => cfg
                    .Attachable()
                    .WithField("DiningCategory",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Dining Category")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Dining Category"))
                    .WithField("DiningAmenities",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Dining Amenities")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Dining Amenities"))
                );

            ContentDefinitionManager.AlterPartDefinition("MeetingFacilityListing",
                cfg => cfg
                    .Attachable()
                    .WithField("MeetingFacilityCategory",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Meeting Facility Category")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Meeting Facility Category"))
                    .WithField("MeetingFacilityAmenities",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Meeting Facility Amenities")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Meeting Facility Amenities"))
                );

            ContentDefinitionManager.AlterPartDefinition("GolfListing",
                cfg => cfg
                    .Attachable()
                    .WithField("GolfAmenities",
                        f => f
                            .OfType("TaxonomyField")
                            .WithDisplayName("Golf Amenities")
                            .WithSetting("TaxonomyFieldSettings.Taxonomy", "Golf Amenities"))
                );

            // assemble parts into five types

            _indexManager.GetSearchIndexProvider().CreateIndex("BusinessListing");

            ContentDefinitionManager.AlterTypeDefinition("AttractionListing",
                cfg => cfg
                    .WithPart("AttractionListing")
                    .WithPart("AttractionListingPart")
                    .WithPart("ContactPart")
                    .WithPart("SeoMetaPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("BodyPart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name: 'Title', Pattern: 'attractions/{Content.Slug}', Description: 'attractions/my-listing'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("PublishLaterPart")
                    .WithPart("IdentityPart")
                    .WithPart("BusinessListingPart")
                    .Draftable()
                    .Creatable()
                    .WithSetting("TypeIndexing.Indexes", "BusinessListing")
                );

            ContentDefinitionManager.AlterTypeDefinition("DiningListing",
                cfg => cfg
                    .WithPart("DiningListing")
                    .WithPart("DiningListingPart")
                    .WithPart("ContactPart")
                    .WithPart("SeoMetaPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("BodyPart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name: 'Title', Pattern: 'dining/{Content.Slug}', Description: 'dining/my-listing'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("PublishLaterPart")
                    .WithPart("IdentityPart")
                    .WithPart("BusinessListingPart")
                    .Draftable()
                    .Creatable()
                    .WithSetting("TypeIndexing.Indexes", "BusinessListing")
                );

            ContentDefinitionManager.AlterTypeDefinition("GolfListing",
                cfg => cfg
                    .WithPart("GolfListing")
                    .WithPart("GolfListingPart")
                    .WithPart("ContactPart")
                    .WithPart("SeoMetaPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("BodyPart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name: 'Title', Pattern: 'golf/{Content.Slug}', Description: 'golf/my-listing'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("PublishLaterPart")
                    .WithPart("IdentityPart")
                    .WithPart("BusinessListingPart")
                    .Draftable()
                    .Creatable()
                    .WithSetting("TypeIndexing.Indexes", "BusinessListing")
                );

            ContentDefinitionManager.AlterTypeDefinition("LodgingListing",
                cfg => cfg
                    .WithPart("LodgingListing")
                    .WithPart("LodgingListingPart")
                    .WithPart("ContactPart")
                    .WithPart("SeoMetaPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("BodyPart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name: 'Title', Pattern: 'lodging/{Content.Slug}', Description: 'lodging/my-listing'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("PublishLaterPart")
                    .WithPart("IdentityPart")
                    .WithPart("BusinessListingPart")
                    .Draftable()
                    .Creatable()
                    .WithSetting("TypeIndexing.Indexes", "BusinessListing")
                );

            ContentDefinitionManager.AlterTypeDefinition("MeetingFacilityListing",
                cfg => cfg
                    .WithPart("MeetingFacilityListing")
                    .WithPart("MeetingFacilityListingPart")
                    .WithPart("ContactPart")
                    .WithPart("SeoMetaPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("BodyPart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name: 'Title', Pattern: 'meeting-facilities/{Content.Slug}', Description: 'meeting-facilities/my-listing'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("PublishLaterPart")
                    .WithPart("IdentityPart")
                    .WithPart("BusinessListingPart")
                    .Draftable()
                    .Creatable()
                    .WithSetting("TypeIndexing.Indexes", "BusinessListing")
                );



            return 1;
        }
    }
}